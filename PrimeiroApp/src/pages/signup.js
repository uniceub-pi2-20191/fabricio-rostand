import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  StatusBar,
  Text,
  TouchableOpacity 
} from 'react-native';

import Logo from '../components/logo';
import Form from '../components/form';
import {Actions} from 'react-native-router-flux'; 

export default class Signup extends Component<{}> {
  
  goBack() {
    Actions.pop();

  }
    render() {
        return(
            <View style = {styles.container}>
                <Logo/>
                <Form type = 'Cadastrar'/>
                <View style = {styles.signupTextCont}>
                    <Text style = {styles.signupText}>Já tem uma conta?</Text>
                    <Text style = {styles.signupButton}>Entrar</Text>
                    <TouchableOpacity onPress = {this.goBack}></TouchableOpacity>
                </View>
            </View>
            )
    }
}        

const styles = StyleSheet.create({ 
    container : {
      backgroundColor: '#4b0082',
      flex: 1,
      alignItems: 'center',
      justifyContent: 'center'
    },
    signupTextCont: {
      flexGrow: 1,
      alignItems: 'flex-end',
      justifyContent: 'center',
      paddingVertical: 16,
      flexDirection: 'row'
    },
    signupText: {
      color: 'rgba(255, 255, 255, 0.6)',
      fontSize: 16
    },
    signupButton: {
      color: '#ffffff',
      fontSize: 16,
      fontWeight: '500'
    }

});     